var static = require('node-static');
var http = require('http');
// host name
var hostname=process.argv[2];
if(typeof hostname=='undefined')
	hostname='0.0.0.0';
console.log("host name  assigned :" +hostname);
//port number
var port=process.argv[3];
if(typeof port=='undefined')
	port=8080;
console.log("port number assigned :" +port);
var file = new(static.Server)();
// server
var server = http.createServer(function (req, res) {
  res.end('Server started \n');
  file.serve(req, res);
});
// listner
var listner=server.listen(port,hostname);
